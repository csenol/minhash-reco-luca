name := "minhash"

organization := "com.lucaongaro"

version := "0.0.1"

licenses ++= Seq("MIT" -> url(s"https://bitbucket.org/lucaong/minhash-reco/raw/v${version.value}/LICENSE"))

scalaVersion := "2.11.0"

scalacOptions += "-feature"

resolvers += "rediscala" at "http://dl.bintray.com/etaty/maven"

libraryDependencies += "org.scalatest" %% "scalatest" % "2.1.6" % "test"

libraryDependencies += "com.etaty.rediscala" %% "rediscala" % "1.3.1"

libraryDependencies += "org.scala-stm" %% "scala-stm" % "0.7"

libraryDependencies += "org.scalatest" %% "scalatest" % "2.1.6" % "test"

libraryDependencies += "org.scalamock" %% "scalamock-scalatest-support" % "3.1.1" % "test"
