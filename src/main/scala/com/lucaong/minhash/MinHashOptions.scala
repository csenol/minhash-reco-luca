package com.lucaongaro.minhash

case class MinHashOptions( numOfHashes: Int, numOfBands: Int, hashesPerBand: Int ) {
  require( hashesPerBand * numOfBands <= numOfHashes,
    "hashesPerBand * numOfBands should be lower or equal than numOfHashes" )
}
