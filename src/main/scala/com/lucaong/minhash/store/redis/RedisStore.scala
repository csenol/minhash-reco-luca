package com.lucaongaro.minhash.store.redis

import _root_.redis.commands.TransactionBuilder
import _root_.redis.{ RedisClient, RedisCommands, ByteStringFormatter }
import com.lucaongaro.minhash._
import com.lucaongaro.minhash.store._
import scala.concurrent._
import scala.concurrent.duration._
import scala.util.{ Success, Failure }

class RedisStore[S](
  redis:     RedisClient = new RedisClient()( akka.actor.ActorSystem() ),
  keyPrefix: String      = "mh"
)( implicit val executionContext: ExecutionContext, bsf: RedisFormatter[S] ) extends Store[S] {

  import RedisFormatter._

  def updateSignature( setId: S, signature: Signature ) =
    atomicUpdate[Signature]( setIdKey( setId ) ) { opt =>
      opt match {
        case None => signature
        case Some( oldSignature ) =>
          oldSignature updateWith signature
      }
    }

  def getSignature( setId: S ) = getSignature( setId, redis )

  def getSignature( setId: S, redis: RedisCommands ): Future[Option[Signature]] =
    redis.get[Signature]( setIdKey( setId ) )

  override def getSignatures( setIds: Seq[S] ) = pipelined { r =>
    for {
      signatures <- Future.traverse( setIds ) { setId => getSignature( setId, r ) }
    } yield ( setIds zip signatures ).view.filter( _._2.isDefined ).map( t => (t._1, t._2.get ) ).toMap
  }

  def putInBuckets( buckets: Map[Int, Hash], setId: S ) = pipelined { r =>
    Future.traverse( buckets ) { case (band, bucket) =>
      r.sadd( bucketKey( band, bucket ), setId )
    }.map( f => () )
  }

  def removeFromBuckets( buckets: Map[Int, Hash], setId: S ) = pipelined { r =>
    Future.traverse( buckets ) { case (band, bucket) =>
      r.srem( bucketKey( band, bucket ), setId )
    }.map( f => () )
  }

  def getInBuckets( buckets: Map[Int, Hash] ) = pipelined { r =>
    Future.traverse( buckets ) { case (band, bucket) =>
      r.smembers[S]( bucketKey( band, bucket ) )
    }.map( _.flatten.toSet )
  }

  def getSeedsOrElseUpdate( seeds: => Seq[Int] ) = {
    val seedsKey = s"$keyPrefix:seeds"
    val s = for {
      done     <- redis.setnx[Seq[Int]]( seedsKey, seeds )
      setSeeds <- redis.get[Seq[Int]]( seedsKey )
    } yield setSeeds.get
    Await.result( s, 3.seconds )
  }

  private def pipelined[T]( block: TransactionBuilder => T ): T = {
    val txn    = redis.transaction()
    val result = block( txn )
    if ( txn.operations.result().nonEmpty ) txn.exec()
    result
  }

  private def atomicUpdate[T]( key: String )( fn: Option[T] => T )
    ( implicit bsf: ByteStringFormatter[T] ): Future[(Option[T], T)] =
    redis.get[T]( key ).flatMap { current =>
      val updated = fn( current )
      if ( current.nonEmpty && current.get == updated )
        Future.successful( (current, updated) )
      else
        redisExt
          .compareAndSwap( key, current.getOrElse( updated ), updated )
          .flatMap { done =>
            if ( done.toString == "OK" ) Future.successful( (current, updated) )
            else atomicUpdate[T]( key )( fn )
          }
    }

  private def redisExt = new RedisClientExt( redis )

  private def bucketKey( band: Int, bucket: Hash ) = s"$keyPrefix:bkt:$band:$bucket"

  private def setIdKey( setId: S )( implicit bsf: RedisFormatter[S] ) =
    s"$keyPrefix:itm:" + bsf.toKey( setId )
}
