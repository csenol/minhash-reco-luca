package com.lucaongaro.minhash

import com.lucaongaro.minhash.store.Store
import scala.collection.immutable.SortedSet
import scala.concurrent._
import scala.util.Random

class MinHash[S, E](
  store:         Store[S],
  numOfHashes:   Int = 200,
  numOfBands:    Int = 30,
  hashesPerBand: Int = 1
)( implicit val executionContext: ExecutionContext, val hashable: Hashable[E] ) extends Engine[S, E] {

  implicit val options = MinHashOptions( numOfHashes, numOfBands, hashesPerBand )

  def addElementToSet( setId: S, setElem: E ): Future[Unit] = {
    for {
      (oldOpt, now) <- store.updateSignature( setId, Signature.calculate( setElem, seeds ) )
    } yield {
      oldOpt match {
        case Some( old ) =>
          val (toRemove, toAdd) = old.toBuckets changes now.toBuckets
          store.putInBuckets( toAdd, setId ) zip store.removeFromBuckets( toRemove, setId )
        case None =>
          store.putInBuckets( now.toBuckets, setId )
      }
    }
  }

  def findNeighborsOf( setId: S, limit: Int = 10, threshold: Double = 0.1 )
    ( implicit o: Ordering[Neighbor[S]] ): Future[SortedSet[Neighbor[S]]] = {
    for {
      optSignature <- store.getSignature( setId )
      candidates   <- optSignature match {
        case None              => Future.successful( Set.empty[Candidate[S]] )
        case Some( signature ) => getCandidates( signature, setId )
      }
    } yield candidates.foldLeft( SortedSet.empty[Neighbor[S]] ) { ( set, candidate ) =>
      set + Neighbor( candidate.setId, optSignature.get similarity candidate.signature )
    }.view.take( limit ).filter( _.similarity > threshold ).force
  }

  private def getCandidates( signature: Signature, setId: S ): Future[Set[Candidate[S]]] = {
    for {
      setIds     <- store.getInBuckets( signature.toBuckets )
      signatures <- store.getSignatures( ( setIds - setId ).toSeq )
    } yield signatures.foldLeft( Set.empty[Candidate[S]] ) { ( set, s ) => set + Candidate( s._1, s._2 ) }
  }

  private val seeds = store.getSeedsOrElseUpdate { (1 to numOfHashes) map Random.nextInt }
}
